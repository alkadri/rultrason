# Rultrason

## Context

Rultrason is a collection of R functions and scripts used for 
the calculation and analysis of mechanical characterizations 
using ultrasound method. Originally written by 
[Eric Rosenkrantz](mailto:eric.rosenkrantz@umontpellier.fr)
in MATLAB and depends on Signal Processing Toolbox, we have 
ported them to with a simple goal: to be able to make the 
calculations and analysis wherever we have to using only 
R and some additional packages (which are all open-source).

The authors of these 
codes, directly or indirectly, are:

*  Ahmad Alkadri ([ahmad.alkadri@umontpellier.fr](mailto:ahmad.alkadri@umontpellier.fr)),
the current developer and maintainer of these codes
*  Eric Rosenkrantz ([eric.rosenkrantz@umontpellier.fr](mailto:eric.rosentkrantz@umontpellier.fr)),
the original developer and writer of these codes, who still actively developing
the scripts now
*  Olivier Arnould
*  Delphine Jullien
*  Joseph Gril

This package is constantly under development for now. We’re still
actively working with the ultrasound technique. Active developments and
updates will be made regularly. Please direct all questions and suggestions 
by e-mail to [ahmad.alkadri@umontpellier.fr](mailto:ahmad.alkadri@umontpellier.fr).

## Dependencies

As mentioned before, the scripts depend solely on three R packages: 
`tcltk` (which is a base-R package, which means we do not have to download 
it again), `pracma` (for additional mathematical functionalities), 
and `seewave` (for signal processing, mostly the hilbert envelope). 
Once the script is launched, if you do not have those packages installed, 
R will promptly download and use them.

## How to Use

Currently, the main script is the one titled `code_vit_xcoor_brut.R`. Once you launch 
it, two dialog boxes will appear consecutively: one asking you to select the 
Reference file and one asking to select the data file. Once those two are selected, 
the calculation will occur, and a file titled `Results_speed_calculated.csv` will appear 
in the same repository of this code, containing the results for the ultrasound 
propagation speed in your measured materials.

This script is currently still being developed, together with the 
methodology used. At the moment, we're in the middle of writing one or two articles
which will describe what the methodology used for the measurement is and 
how to use this script for the calculation. As soon as they are published, 
we will inform about it here.
